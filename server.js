////////////////////////
//// nodejs project ////
////////////////////////

// This is an example of how to import code from a package
var express = require("express");

// In order to use this, we have to make sure it's installed first.  This is what I ran from the command line:
// $ npm install express --save
// This installs the "express" library so we can use its functions by storing it in the variable "express", and saves it 
// to our package.json file, so we can run:
// $ npm install
// to install all of our dependencies.



// Here's where we build a new instance of a server, which we'll call "app":
var app = new express();


// And next, we'll 'route' all traffic to the main site '/' to execute this function to print "hi"
app.get("/", function(req, res) {
  
  console.log("someone requested our page");
  res.send("hello world");
  
});

app.get("/dog", function(req, res) {
  
  console.log("someone requested our page");
  res.send("Woof Woof");
  
});

app.get("/cat", function(req, res) {
  
  console.log("someone requested our page");
  res.send("Meow Nigga");
  
});

app.post("/bob", function(req, res) {
  res.send("bob");
});




// and then we'll start it on port 8005.  To check, got to http://localhost:8005
// You should see the phrase "hi" printed each time you refresh.
app.listen(8005, function() {
  
  console.log("listening on port 8005");
  
});

// fixme i am broken